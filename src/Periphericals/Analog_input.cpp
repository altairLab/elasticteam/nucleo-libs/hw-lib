#include "hw/Periphericals/Analog_input.h"

uint8_t Analog_Input::_using_ADC1 = 0;
uint8_t Analog_Input::_using_ADC2 = 0;
uint8_t Analog_Input::_using_ADC3 = 0;
uint16_t *Analog_Input::_pointer = NULL;
uint32_t Analog_Input::_Size_buffer_v[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
uint32_t Analog_Input::_Convertion_time_buffer[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
uint8_t Analog_Input::_Number_convertions = 0;
uint32_t Analog_Input::_Total_buffer = 0;
uint32_t Analog_Input::_Max_buffer = 0;
uint32_t Analog_Input::_ADC_Clock = 0;

Analog_Input::~Analog_Input() {
    #ifdef TARGET_STM32L4
        #if defined(ADC1)
            if ((ADCName &)_Conversor == ADC_1) {
                _using_ADC1--;
            }
        #endif
        #if defined(ADC2)
            if ((ADCName &)_Conversor == ADC_2) {
                _using_ADC2--;
            }
        #endif
    #elif TARGET_STM32F4
        #if defined(ADC1)
            if ((ADCName &)_Conversor == ADC_1) {
                _using_ADC1--;
            }
        #endif
        #if defined(ADC2)
            if ((ADCName &)_Conversor == ADC_2) {
                _using_ADC2--;
            }
        #endif
        #if defined(ADC3)
            if ((ADCName &)_Conversor == ADC_3) {
                _using_ADC3--;
            }
        #endif
    #endif

    if (_Size_buffer != 0) free(_pointer);
}

//Analog_Input::Analog_Input(PinName pin) {
ADCError Analog_Input::init() {
    ADCError error = ADC_OK;
    bool first_instance;

    //default setting for ADC
    ADCPrescaler Prescaler = ADC_SPCLK1;
    ADCAlign Alignment = ADC_Right;
    ADCResolution Resolution = ADC_12b;
    ADCSample Sample = ADC_2s5;

    ADC_Common_TypeDef ADC_Common;
    uint32_t function = pinmap_function(pin, PinMap_ADC);
    //find for ADC (1, 2 or 3)
    _Conversor = (ADC_TypeDef *)pinmap_peripheral(pin, PinMap_ADC);
    //configure the GPIO
    pinmap_pinout(pin, PinMap_ADC);
    //find for channel (1, 2, ... or 15)
    _Channel = STM_PIN_CHANNEL(function);

    first_instance = check_adc();

    if (first_instance)
    {
        //configure the clock prescaler of the ADC
        check_prescaler(Prescaler);
        ADC_Common.CCR = Prescaler;
    }

    _Index_Buffer = 0;
    config_sample(Sample);
    check_clock(Prescaler);
    check_convertion_time(Sample, Resolution);

    //configure the ADC
    #ifdef TARGET_STM32L4
        if (first_instance) _Conversor->CFGR = Resolution | Alignment;
        _Conversor->CFGR2 = 0U;
    #elif TARGET_STM32F4
        if (first_instance)
        {
            _Conversor->CR1 = Resolution;
            _Conversor->CR2 = Alignment;
        }
    #endif

    _continuous_mode = false;
    _usage_dma = false;
    _usage_fast_divider = false;
    _injection_convertion = true;

    //set initializated flag
    _initialized = true;

    return error;
}

//Analog_Input::Analog_Input(PinName pin, ADCPrescaler Prescaler, ADCAlign Alignment, ADCSample Sample, ADCResolution Resolution) {
ADCError Analog_Input::init(ADCPrescaler Prescaler, ADCAlign Alignment, ADCSample Sample, ADCResolution Resolution) {
    ADCError error = ADC_OK;
    bool first_instance;
    ADC_Common_TypeDef ADC_Common;
    uint32_t function = pinmap_function(pin, PinMap_ADC);
    //find for ADC (1, 2 or 3)
    _Conversor = (ADC_TypeDef *)pinmap_peripheral(pin, PinMap_ADC);
    //configure the GPIO
    pinmap_pinout(pin, PinMap_ADC);
    //find for channel (1, 2, ... or 15)
    _Channel = STM_PIN_CHANNEL(function);
    
    first_instance = check_adc();

    if (first_instance) 
    {
        //configure the clock prescaler of the ADC
        check_prescaler(Prescaler);
        ADC_Common.CCR = Prescaler;
    }

    _Index_Buffer = 0;
    config_sample(Sample);
    check_clock(Prescaler);
    check_convertion_time(Sample, Resolution);

    //configure the ADC
    #ifdef TARGET_STM32L4
        if (first_instance) _Conversor->CFGR = Resolution | Alignment;
        _Conversor->CFGR2 = 0U;
    #elif TARGET_STM32F4
        if (first_instance)
        {
            _Conversor->CR1 = Resolution;
            _Conversor->CR2 = Alignment;
        }
    #endif

    _continuous_mode = false;
    _usage_dma = false;
    _usage_fast_divider = false;
    _injection_convertion = true;

    //set initializated flag
    _initialized = true;

    return error;
}

//Analog_Input::Analog_Input(PinName pin, ADCPrescaler Prescaler, ADCAlign Alignment, ADCSample Sample, ADCResolution Resolution, ADCContinuous Continuous, ADCDma Dma, uint32_t Size_buffer) {
ADCError Analog_Input::init(ADCPrescaler Prescaler, ADCAlign Alignment, ADCSample Sample, ADCResolution Resolution, ADCContinuous Continuous, ADCDma Dma, uint32_t Size_buffer) {
    ADCError error = ADC_OK;
    bool first_instance;
    uint32_t i, j, k;
    ADC_Common_TypeDef ADC_Common;
    uint16_t *_new_pointer = NULL;
    uint32_t function = pinmap_function(pin, PinMap_ADC);
    //find for ADC (1, 2 or 3)
    _Conversor = (ADC_TypeDef *)pinmap_peripheral(pin, PinMap_ADC);
    //configure the GPIO
    pinmap_pinout(pin, PinMap_ADC);
    //find for channel (1, 2, ... or 15)
    _Channel = STM_PIN_CHANNEL(function);
    
    first_instance = check_adc();

    if ((_Conversor->CR & ADC_CR_ADEN) == ADC_CR_ADEN) return ADC_ERROR_IS_ENABLE;

    if (first_instance)
    {
        //configure the clock prescaler of the ADC
        check_prescaler(Prescaler);
        ADC_Common.CCR = Prescaler;
    }

    config_sample(Sample);
    check_clock(Prescaler);
    check_convertion_time(Sample, Resolution);
    
    //check if using DMA
    _Index_Buffer = 0;
    if (Size_buffer > (65535/2)) return ADC_ERROR_BUFFER_TOO_MUCH;
    else if (Size_buffer == 0) return ADC_ERROR_BUFFER_IS_ZERO;
    else _Size_buffer = Size_buffer;
    if (Dma == ADC_Dma)
    {
        _Index_Buffer = _Number_convertions;
        _Size_buffer_v[_Index_Buffer] = _Size_buffer;
        if (_Number_convertions == 0)
        {
            _Max_buffer = _Size_buffer;
            _new_pointer = (uint16_t *)malloc((_Max_buffer*2)*2);
            if (_new_pointer == NULL) return ADC_ERROR_MEMORY_NOT_ENOUGH;
            else _pointer = _new_pointer;
            _Number_convertions = 1;
        }
        else
        {
            _Number_convertions++;
            if (_Size_buffer > _Max_buffer) _Max_buffer = _Size_buffer;
            _new_pointer = (uint16_t *)realloc(_pointer, (_Max_buffer*2)*2*_Number_convertions);
            if (_new_pointer == NULL) return ADC_ERROR_MEMORY_NOT_ENOUGH;
            else _pointer = _new_pointer;
        }
        _Total_buffer = _Max_buffer*_Number_convertions;
        _Convertion_time_buffer[_Index_Buffer] = _Convertion_time;

        #ifdef TARGET_STM32L4
            #if defined(ADC1)
                if ((ADCName &)_Conversor == ADC_1)
                {
                    if ((DMA1_Channel1->CCR & DMA_CCR_EN) == 0)
                    {
                        if(__DMA1_IS_CLK_DISABLED()) __DMA1_CLK_ENABLE();
                        _Stream_DMA = DMA1_Channel1;
                        DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & 0xFFFFFFF0) | (DMA_REQUEST_0<<0);
                    }
                    else if ((DMA1_CSELR->CSELR & DMA_REQUEST_0<<0) == (DMA_REQUEST_0<<0)) return ADC_ERROR_DMA_IS_ENABLE;
                    else if ((DMA2_Channel3->CCR & DMA_CCR_EN) == 0)
                    {
                        if(__DMA2_IS_CLK_DISABLED()) __DMA2_CLK_ENABLE();
                        _Stream_DMA = DMA2_Channel3;
                        DMA2_CSELR->CSELR = (DMA2_CSELR->CSELR & 0xFFFFF0FF) | (DMA_REQUEST_0<<8);
                    }
                    else if ((DMA2_CSELR->CSELR & DMA_REQUEST_0<<8) == (DMA_REQUEST_0<<8)) return ADC_ERROR_DMA_IS_ENABLE;
                    else return ADC_ERROR_DMA_NOT_AVAILABLE;
                }
            #endif
            #if defined(ADC2)
                if ((ADCName &)_Conversor == ADC_2)
                {
                    if ((DMA1_Channel2->CCR & DMA_CCR_EN) == 0)
                    {
                        if(__DMA1_IS_CLK_DISABLED()) __DMA1_CLK_ENABLE();
                        _Stream_DMA = DMA1_Channel2;
                        DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & 0xFFFFFF0F) | (DMA_REQUEST_0<<4);
                    }
                    else if ((DMA1_CSELR->CSELR & DMA_REQUEST_0<<4) == (DMA_REQUEST_0<<4)) return ADC_ERROR_DMA_IS_ENABLE;
                    else if ((DMA2_Channel4->CCR & DMA_CCR_EN) == 0)
                    {
                        if(__DMA2_IS_CLK_DISABLED()) __DMA2_CLK_ENABLE();
                        _Stream_DMA = DMA2_Channel4;
                        DMA2_CSELR->CSELR = (DMA2_CSELR->CSELR & 0xFFFF0FFF) | (DMA_REQUEST_0<<12);
                    }
                    else if ((DMA2_CSELR->CSELR & DMA_REQUEST_0<<12) == (DMA_REQUEST_0<<12)) return ADC_ERROR_DMA_IS_ENABLE;
                    else return ADC_ERROR_DMA_NOT_AVAILABLE;
                }
            #endif
            _Stream_DMA->CCR = DMA_CCR_CIRC | DMA_CCR_MINC | DMA_CCR_PSIZE_0 | DMA_CCR_MSIZE_0 | DMA_CCR_PL_0;
            _Stream_DMA->CNDTR = _Total_buffer*2;
            _Stream_DMA->CPAR = (uint32_t)&_Conversor->DR;
            _Stream_DMA->CMAR = (uint32_t)_pointer;
        #elif TARGET_STM32F4
            if (__DMA2_IS_CLK_DISABLED()) __DMA2_CLK_ENABLE();
            #if defined(ADC1)
                if ((ADCName &)_Conversor == ADC_1)
                {
                    if ((DMA2_Stream0->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream0;
                        _Stream_DMA->CR = 0U;
                    }
                    else if ((DMA2_Stream4->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream4;
                        _Stream_DMA->CR = 0U;
                    }
                }
            #endif
            #if defined(ADC2)
                if ((ADCName &)_Conversor == ADC_2) {
                    if ((DMA2_Stream2->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream2;
                        _Stream_DMA->CR = 0x02000000;
                    }
                    else if ((DMA2_Stream3->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream3;
                        _Stream_DMA->CR = 0x02000000;
                    }
                }
            #endif
            #if defined(ADC3)
                if ((ADCName &)_Conversor == ADC_3) {
                    if ((DMA2_Stream0->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream0;
                        _Stream_DMA->CR = 0x04000000;
                    }
                    else if ((DMA2_Stream1->CR & 0x1) == 0)
                    {
                        _Stream_DMA = DMA2_Stream1;
                        _Stream_DMA->CR = 0x04000000;
                    }
                }
            #endif
            _Stream_DMA->CR |= 0x00032D00;
            _Stream_DMA->NDTR = _Size_buffer;
            _Stream_DMA->PAR = (uint32_t)&_Conversor->DR;
            _Stream_DMA->M0AR = (uint32_t)_pointer;
        #endif
        Continuous = ADC_Continuous;
    }

    #ifdef TARGET_STM32L4
    //configure the channel for convertion
        _Conversor->SQR1 = (_Conversor->SQR1 & 0xFFFFFFF0) | (_Index_Buffer & 0x0000000F);
        switch (_Index_Buffer)
        {
            case 0:
                _Conversor->SQR1 |= (_Conversor->SQR1 & 0xFFFFF83F) | _Channel<<6;
                break;
            case 1:
                _Conversor->SQR1 |= (_Conversor->SQR1 & 0xFFFE0FFF) | _Channel<<12;
                break;
            case 2:
                _Conversor->SQR1 |= (_Conversor->SQR1 & 0xFF83FFFF) | _Channel<<18;
                break;
            case 3:
                _Conversor->SQR1 |= (_Conversor->SQR1 & 0xE0FFFFFF) | _Channel<<24;
                break;
            case 4:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFFFFE0) | _Channel<<0;
                break;
            case 5:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFFF83F) | _Channel<<6;
                break;
            case 6:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFE0FFF) | _Channel<<12;
                break;
            case 7:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFF83FFFF) | _Channel<<18;
                break;
            case 8:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xE0FFFFFF) | _Channel<<24;
                break;
            case 9:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFFFFFFE0) | _Channel<<0;
                break;
        }
        _Conversor->SQR4 = 0U;
    #elif TARGET_STM32F4
        //configure the channel for convertion
        _Conversor->SQR1 = (_Conversor->SQR1 & 0xFF0FFFFF) | ((_Index_Buffer<<20) & 0x00F00000);
        switch (_Index_Buffer)
        {
            case 0:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFFFFFFE0) | _Channel<<0;
                break;
            case 1:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFFFFFC1F) | _Channel<<5;
                break;
            case 2:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFFFF83FF) | _Channel<<10;
                break;
            case 3:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFFF07FFF) | _Channel<<15;
                break;
            case 4:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xFE0FFFFF) | _Channel<<20;
                break;
            case 5:
                _Conversor->SQR3 |= (_Conversor->SQR3 & 0xC1FFFFFF) | _Channel<<25;
                break;
            case 6:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFFFFE0) | _Channel<<0;
                break;
            case 7:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFFFC1F) | _Channel<<5;
                break;
            case 8:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFFF83FF) | _Channel<<10;
                break;
            case 9:
                _Conversor->SQR2 |= (_Conversor->SQR2 & 0xFFF07FFF) | _Channel<<15;
                break;
        }
        _Conversor->SQR1 = 0U;
    #endif

    //configure the ADC
    #ifdef TARGET_STM32L4
        if (first_instance) _Conversor->CFGR = Dma | Resolution | Alignment | Continuous;
        else _Conversor->CFGR |= (Dma | Continuous);
        _Conversor->CFGR2 = 0U;
    #elif TARGET_STM32F4
        if (first_instance)
        {
            _Conversor->CR1 = Resolution;
            _Conversor->CR2 = Alignment | Continuos | Dma;
        }
        else _Conversor->CR2 |= (Continuos | Dma);
    #endif

    //check configuration
    if (Continuous == ADC_Continuous) _continuous_mode = true;
    else _continuous_mode = false;
    if (Dma == ADC_Dma) _usage_dma = true;
    else _usage_dma = false;
    
    //determine the divider coeficiente
    k = _Size_buffer;
    j = 0;
    for (i = 0; i < 32; i++)
    {
        if ((k & 0x1) != 0) j++;
        k>>1;
    }
    if (j > 1)
    {
        _usage_fast_divider = false;
        _Divider = _Size_buffer;
    }
    else 
    {
        _usage_fast_divider = true;
        k = _Size_buffer;
        _Divider = 0;
        do
        {
            k = k>>1;
            _Divider++;
        } while ((k & 0x1) == 0);
    }

    _injection_convertion = false;

    //set initializated flag
    _initialized = true;

    return error;
}

bool Analog_Input::check_adc()
{
    bool _first_instance = false;
    //enable the clock of the ADC
    #ifdef TARGET_STM32L4
        #if defined(ADC1)
            if ((ADCName &)_Conversor == ADC_1) {
                __HAL_RCC_ADC_CLK_ENABLE();
                if (_using_ADC1 == 0) _first_instance = true;
                else _first_instance = false;
                _using_ADC1++;
            }
        #endif
        #if defined(ADC2)
            if ((ADCName &)_Conversor == ADC_2) {
                __HAL_RCC_ADC_CLK_ENABLE();
                if (_using_ADC2 == 0) _first_instance = true;
                else _first_instance = false;
                _using_ADC2++;
            }
        #endif

    #elif TARGET_STM32F4
        #if defined(ADC1)
            if ((ADCName &)_Conversor == ADC_1) {
                __HAL_RCC_ADC1_CLK_ENABLE();
                if (_using_ADC1 == 0) _first_instance = true;
                else _first_instance = false;
                _using_ADC1++;
            }
        #endif
        #if defined(ADC2)
            if ((ADCName &)_Conversor == ADC_2) {
                __HAL_RCC_ADC2_CLK_ENABLE();
                if (_using_ADC2 == 0) _first_instance = true;
                else _first_instance = false;
                _using_ADC2++;
            }
        #endif
        #if defined(ADC3)
            if ((ADCName &)_Conversor == ADC_3) {
                __HAL_RCC_ADC3_CLK_ENABLE();
                if (_using_ADC3 == 0) _first_instance = true;
                else _first_instance = false;
                _using_ADC3++;
            }
        #endif
    #endif

    return _first_instance;
}

void Analog_Input::config_sample(ADCSample Sample)
{
    #ifdef TARGET_STM32L4
        if ((pin != A0) && (Sample == ADC_2s5)) Sample = ADC_6s5;
        //configure the time sample
        if (_Channel < 10)
        {
            _Conversor->SMPR1 = Sample<<(_Channel*3);
            //_Conversor->SMPR2 = 0U;
        }
        else
        {
            //_Conversor->SMPR1 = 0U;
            _Conversor->SMPR2 = Sample<<(_Channel*3);
        }
    #elif TARGET_STM32F4
        //configure the time sample
        if (_Channel < 10)
        {
            _Conversor->SMPR1 = 0U;
            _Conversor->SMPR2 = Sample<<(_Channel*3);
        }
        else
        {
            _Conversor->SMPR1 = Sample<<(_Channel*3);
            _Conversor->SMPR2 = 0U;
        }
    #endif
}

void Analog_Input::check_prescaler(ADCPrescaler Prescaler)
{
    #ifdef TARGET_STM32L4
        if ((Prescaler == ADC_SPCLK1) || (Prescaler == ADC_SPCLK2) || (Prescaler == ADC_SPCLK4)) {
            RCC->CCIPR |= RCC_ADCCLKSOURCE_SYSCLK;
        }
        else {
            RCC->CCIPR |= RCC_ADCCLKSOURCE_PLLSAI1;
            RCC->PLLSAI1CFGR |= RCC_PLLSAI1CFGR_PLLSAI1REN;
            RCC->PLLCFGR |= RCC_PLLCFGR_PLLPEN;
            __HAL_RCC_PLLSAI1_ENABLE();
        }
    #endif
}

void Analog_Input::check_clock(ADCPrescaler Prescaler)
{
    _ADC_Clock = HAL_RCCEx_GetPeriphCLKFreq(RCC_PERIPHCLK_ADC);
    switch(Prescaler)
    {
        case ADC_PCLK2:
            _ADC_Clock /= 2;
            break;
        case ADC_PCLK4:
            _ADC_Clock /= 4;
            break;
        case ADC_PCLK6:
            _ADC_Clock /= 6;
            break;
        case ADC_PCLK8:
            _ADC_Clock /= 8;
            break;
        #ifdef TARGET_STM32L4
            case ADC_PCLK1:
                _ADC_Clock /= 1;
                break;
            case ADC_PCLK10:
                _ADC_Clock /= 10;
                break;
            case ADC_PCLK12:
                _ADC_Clock /= 12;
                break;
            case ADC_PCLK16:
                _ADC_Clock /= 16;
                break;
            case ADC_PCLK32:
                _ADC_Clock /= 32;
                break;
            case ADC_PCLK64:
                _ADC_Clock /= 64;
                break;
            case ADC_PCLK128:
                _ADC_Clock /= 128;
                break;
            case ADC_PCLK256:
                _ADC_Clock /= 256;
                break;
            case ADC_SPCLK1:
                _ADC_Clock /= 1;
                break;
            case ADC_SPCLK2:
                _ADC_Clock /= 2;
                break;
            case ADC_SPCLK4:
                _ADC_Clock /= 4;
                break;
        #endif
    }
}

void Analog_Input::check_convertion_time(ADCSample Sample, ADCResolution Resolution)
{
    float _resolution;
    float _sample;
    float _ts;

    #ifdef TARGET_STM32L4
        switch (Resolution)
        {
            case ADC_6b:
                _resolution = 6.5;
                break;
            case ADC_8b:
                _resolution = 8.5;
                break;
            case ADC_10b:
                _resolution = 10.5;
                break;
            case ADC_12b:
                _resolution = 12.5;
                break;
        }

        switch (Sample)
        {
            case ADC_2s5:
                _sample = 2.5;
                break;
            case ADC_6s5:
                _sample = 6.5;
                break;
            case ADC_12s5:
                _sample = 12.5;
                break;
            case ADC_24s5:
                _sample = 24.5;
                break;
            case ADC_47s5:
                _sample = 47.5;
                break;
            case ADC_92s5:
                _sample = 92.5;
                break;
            case ADC_247s5:
                _sample = 247.5;
                break;
            case ADC_640s5:
                _sample = 640.5;
                break;
        }
    #elif TARGET_STM32F4
        switch (Resolution)
        {
            case ADC_6b:
                _resolution = 6;
                break;
            case ADC_8b:
                _resolution = 8;
                break;
            case ADC_10b:
                _resolution = 10;
                break;
            case ADC_12b:
                _resolution = 12;
                break;
        }

        switch (Sample)
        {
            case ADC_3s:
                _sample = 3.0;
                break;
            case ADC_15s:
                _sample = 15.0;
                break;
            case ADC_28s:
                _sample = 28.0;
                break;
            case ADC_56s:
                _sample = 56.0;
                break;
            case ADC_84s:
                _sample = 84.0;
                break;
            case ADC_112s:
                _sample = 112.0;
                break;
            case ADC_144s:
                _sample = 144.0;
                break;
            case ADC_480s:
                _sample = 480.0;
                break;
        }
    #endif
    
    _ts = (_sample + _resolution)/((float)_ADC_Clock);

    _Convertion_time = (uint32_t)(_ts * 1000000000);
}

int8_t Analog_Input::enable()
{
    u_int32_t timeout = 0;
    if (_initialized) //Check if the ADC was initialized
    {
        #ifdef TARGET_STM32L4
            if (is_enabled() == 0) //Check if the ADC is enable
            {
                if (_usage_dma) _Stream_DMA->CCR |= DMA_CCR_EN;
                _Conversor->CR &= ~ADC_CR_DEEPPWD; //Ensure the ADC out deep power
                _Conversor->CR |= ADC_CR_ADVREGEN; //Enable the ADC regulator
                wait_us(25); //Wait for regulater power on
                _Conversor->CR |= ADC_CR_ADCAL; //Start calibration sequence
                timeout = 5000000;
                while ((_Conversor->CR & ADC_CR_ADCAL) != 0U) //Wait for calibration
                {
                    timeout--;
                    if (timeout == 0) return(-2);
                }
                wait_us(1); //Ensure time after calibration
                _Conversor->ISR |= ADC_ISR_ADRDY; //Reset bit ADC enable
                _Conversor->CR |= ADC_CR_ADEN; //Start enable sequence
                timeout = 5000000;
                while ((_Conversor->ISR & ADC_ISR_ADRDY) != 0U) //Wait for enable is complited
                {
                    timeout--;
                    if (timeout == 0) return(-3);
                }
            }
            else
            {
                if (_continuous_mode && is_started() == 0)
                {
                    start();
                    return(2);
                }
                return(1); //ADC already is enable
            }
        #elif TARGET_STM32F4
            if (_usage_dma) _Stream_DMA->CR |= 1;
            _Conversor->CR2 |= 0x00000001; //Enable the ADC
        #endif
        if (_continuous_mode) //Check if the continuous mode is active
            start(); //Start the convertion
        return(0); //Enabled with success
    }
    else return(-1); //ADC not initialized
}

int8_t Analog_Input::unable()
{
    if (_initialized) //Check if the ADC was initialized
    {
        stop(); //Stop the convertion
        #ifdef TARGET_STM32L4
            if (is_enabled() == 1) //Check if the ADC is unable
            {
                _Conversor->CR |= ADC_CR_ADDIS; //Start unable sequence
                while (is_enabled() == 1); //Wait for unable is complited
            }
            else return(1); //ADC already is unable
        #elif TARGET_STM32F4
            _Conversor->CR2 &= 0xFFFFFFFE; //Unable the ADC
        #endif
        return(0); //Unabled with success
    }
    else return(-1); //ADC not initialized

}

int8_t Analog_Input::start()
{
    if (_initialized) //Check if the ADC was initialized
    {
        #ifdef TARGET_STM32L4
            if (is_enabled() == 1) //Check if the ADC is enable
            {
                if (is_started() == 0) //Check if the conversion is in progress
                {
                    if (_injection_convertion)
                    {
                        _Conversor->CFGR |= (ADC_CFGR_JQDIS | ADC_CFGR_JDISCEN);
                        _Conversor->JSQR = _Channel<<8;
                        _Conversor->CR |= ADC_CR_JADSTART;
                    }
                    else
                    {
                        _Conversor->CR |= ADC_CR_ADSTART; //Start conversion
                    }
                }
                else
                    return(1); //Conversion already is in progress
            }
            else
                return(-2); //ADC not enabled
        #elif TARGET_STM32F4
            _Conversor->CR2 |= 0x40000000; //Start convertion
        #endif
        return(0); //Conversion started with success
    }
    else return(-1); //ADC not initialized
}

int8_t Analog_Input::stop()
{
    if (_initialized) //Check if the ADC was initialized
    {
        #ifdef TARGET_STM32L4
            if (is_started() == 1) //Check if have the conversion is in progress
            {
                if (_injection_convertion)
                {
                    _Conversor->CR |= ADC_CR_JADSTP;
                    while((_Conversor->CR & ADC_CR_JADSTP) != 0U);
                }
                else
                {
                    _Conversor->CR |= ADC_CR_ADSTP; //Start stop conversion sequence
                    while((_Conversor->CR & ADC_CR_ADSTP) != 0U); //Wait finish stop sequence
                }
            }
        #elif TARGET_STM32F4
            _Conversor->CR2 &= 0xBFFFFFFF; //Stop convertion
        #endif
        return(0); //Convertion stoped with success
    }
    else return(-1); //ADC not initialized
}

uint8_t Analog_Input::is_enabled()
{
    #ifdef TARGET_STM32L4
        if ((_Conversor->CR & ADC_CR_ADEN) != 0U) //Check if the ADC is enable
            return 1; //ADC is enable
    #elif TARGET_STM32F4
        if ((_Conversor->CR2 & 0x00000001) != 0U) //Check if the ADC is enable
            return 1; //ADC is enable
    #endif
    else
        return 0; //ADC is unable
}

uint8_t Analog_Input::is_started()
{
    #ifdef TARGET_STM32L4
        if (_injection_convertion)
        {
            if ((_Conversor->CR & ADC_CR_JADSTART) != 0U) //Check if the conversion is in progress
                return 1; //Convertion is in progress
        }
        else
        {
            if ((_Conversor->CR & ADC_CR_ADSTART) != 0U) //Check if the conversion is in progress
                return 1; //Convertion is in progress
        }
    #elif TARGET_STM32F4
        if ((_Conversor->SR & 0x00000010) != 0) //Check if the conversion is in progress
            return 1; //Convertion is in progress
    #endif
    return 0; //Convertion isn't in progress
}

uint8_t Analog_Input::data_ready()
{
    #ifdef TARGET_STM32L4
        if (_injection_convertion)
        {
            if ((_Conversor->ISR & ADC_ISR_JEOC) != 0U)
                return 1;
        }
        else
        {
            if ((_Conversor->ISR & ADC_ISR_EOC) != 0U) //Check if the conversion completed
                return 1; //Conversion completed
        }
    #elif TARGET_STM32F4
        if ((_Conversor->SR & 0x00000002) != 0) //Check if the conversion completed
            return 1; //Conversion completed
    #endif
        return 0; //Conversion didn't complete
}

uint16_t Analog_Input::read_converted_value()
{
    if (_injection_convertion) return _Conversor->JDR1;
    else return _Conversor->DR; //Read the converted value
}

void Analog_Input::reset_complete_flag()
{
    #ifdef TARGET_STM32L4
        if (_injection_convertion) _Conversor->ISR |= ADC_ISR_JEOC;
        else _Conversor->ISR |= ADC_ISR_EOC; //Reset the complete conversion flag
    #elif TARGET_STM32F4
        _Conversor->SR &= 0xFFFFFFFD; //Reset the complete conversion flag
    #endif
}

float Analog_Input::read_average_float()
{
    uint16_t average = 0;
    if (_injection_convertion) average = read_last_word();
    else average = read_average_word();
    if ((_Conversor->CFGR & ADC_CFGR_ALIGN) != 0) return ((float)average)/((float)0xFFFF);
    else return ((float)average)/((float)0x0FFF);
}

uint32_t Analog_Input::read_average_word()
{
    uint32_t i;
    uint32_t sum = 0;
    uint32_t average = 0;
    uint32_t offset = 0;

    if (is_enabled() == 0) return 0;

    if (_injection_convertion)
    {
        average = read_last_word();
    }
    else if (_usage_dma)
    {
        #ifdef TARGET_STM32L4
            if (_Stream_DMA->CNDTR > _Total_buffer) offset = _Total_buffer;
            else offset = 0;
        #elif TARGET_STM32F4
            if (_Stream_DMA->NDTR > _Total_buffer) offset = _Total_buffer;
            else offset = 0;
        #endif
        for (i = 0; i < _Size_buffer; i++) sum += _pointer[offset + _Index_Buffer + i*_Number_convertions];
        if (_usage_fast_divider) average = sum>>_Divider;
        else average = sum/_Divider;
    }
    else if (_continuous_mode)
    {
        average = read_converted_value();
        reset_complete_flag();
    }
    else
    {
        for (i = 0; i < _Size_buffer; i++) sum += read_last_word();
        if (_usage_fast_divider) average = sum>>_Divider;
        else average = sum/_Divider;
    }
    return(average);
}

float Analog_Input::read_last_float()
{
    uint16_t last_value = read_last_word();
    if ((_Conversor->CFGR & ADC_CFGR_ALIGN) != 0) return ((float)last_value)/((float)0xFFFF);
    else return ((float)last_value)/((float)0x0FFF);
}

uint16_t Analog_Input::read_last_word()
{
    uint16_t last_value;
    //uint32_t last_pointer;
    //uint32_t actual_buffer_position;
    uint32_t offset = 0;

    if (is_enabled() == 0) return 0;

    //#ifdef TARGET_STM32L4
    //    actual_buffer_position = _Stream_DMA->CNDTR;
    //#elif TARGET_STM32F4
    //    actual_buffer_position = _Stream_DMA->NDTR;
    //#endif

    if (_usage_dma)
    {
        #ifdef TARGET_STM32L4
            if (_Stream_DMA->CNDTR > _Total_buffer) offset = _Total_buffer;
            else offset = 0;
        #elif TARGET_STM32F4
            if (_Stream_DMA->NDTR > _Total_buffer) offset = _Total_buffer;
            else offset = 0;
        #endif
        //last_pointer = _Total_buffer - actual_buffer_position;
        //if (last_pointer == 0) last_pointer = _Size_buffer-1;
        //else last_pointer -= 1;
        //last_value = _pointer[last_pointer];
        last_value = _pointer[offset + (_Total_buffer - 1) - _Number_convertions + _Index_Buffer];
    }
    else if (_continuous_mode)
    {
        last_value = read_converted_value();
        reset_complete_flag();
    }
    else
    {
        start();
        while (data_ready() == 0);
        last_value = read_converted_value();
        reset_complete_flag();
    }
    return last_value;
}

uint32_t Analog_Input::get_adc_clock()
{
    return _ADC_Clock;
}

uint32_t Analog_Input::get_convertion_time()
{
    return _Convertion_time;
}

uint32_t Analog_Input::get_sample_frequency()
{
    float frequency;
    float time = ((float)get_buffer_fill_time())*0.000000001f;
    time /= (float)_Max_buffer;
    frequency = 1.0f/time;
    return (uint32_t)frequency;
}

uint32_t Analog_Input::get_buffer_fill_time()
{
    uint8_t i;
    uint32_t temp = 0;
    for (i = 0 ; i < 10; i++)
    {
        temp += _Convertion_time_buffer[i];
    }
    return temp * _Max_buffer;
}
